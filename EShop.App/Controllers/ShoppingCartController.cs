﻿using EShop.App.Data;
using EShop.App.Models.DTO;
using EShop.App.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EShop.App.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<EShopApplicationUser> _userManager;

        public ShoppingCartController(ApplicationDbContext context, UserManager<EShopApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            // get the id of the logged in user
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            //get the logged in user
            var loggedInUser = await _context.Users.
                Where(z => z.Id.Equals(userId)).
                Include(z=> z.UserCart).
                Include(z=>z.UserCart.ProductInShoppingCarts).
                Include("UserCart.ProductInShoppingCarts.Product").
                FirstOrDefaultAsync();

            var userShoppingCart = loggedInUser.UserCart;

            var productPrice = userShoppingCart.ProductInShoppingCarts.Select(z => new
            {
                ProductPrice = z.Product.ProductPrice,
                ProductQuantity = z.Quantity
            }).ToList();

           var totalPrice = 0;

           foreach( var item in productPrice)
           {
                totalPrice += item.ProductPrice * item.ProductQuantity;
           }

            // var allProducts = userShoppingCart.ProductInShoppingCarts.Select(z => z.Product).ToList();
            ShoppingCartDto model = new ShoppingCartDto
            {
                Products = userShoppingCart.ProductInShoppingCarts.ToList(),
                TotalPrice = totalPrice
            };



            return View(model);
        }
    }
}
