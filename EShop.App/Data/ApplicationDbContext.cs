﻿using EShop.App.Models.Domain;
using EShop.App.Models.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EShop.App.Data
{
    public class ApplicationDbContext : IdentityDbContext<EShopApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ShoppingCart> ShoppingCarts { get; set; }

        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<ProductInShoppingCart> ProductInShoppingCarts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //composite key for Product in ShoppingCart

            builder.Entity<Product>()
                .Property(z => z.Id)
                .ValueGeneratedOnAdd();

            builder.Entity<ShoppingCart>()
                .Property(z => z.Id)
                .ValueGeneratedOnAdd();

            builder.Entity<ProductInShoppingCart>().HasKey(z => new { z.ProductId, z.ShoppingCartId });

            // relation #1 shoppingCart M - N Product, we get the mutual table
            builder.Entity<ProductInShoppingCart>().HasOne(z => z.Product).
                WithMany(z => z.ProductInShoppingCarts).HasForeignKey(z => z.ShoppingCartId);

            builder.Entity<ProductInShoppingCart>().HasOne(z => z.ShoppingCart).WithMany(
                z => z.ProductInShoppingCarts).HasForeignKey(z => z.ProductId);

            // relation #2 ShoppingCart - User
            builder.Entity<ShoppingCart>().HasOne(z => z.Owner).WithOne(z => z.UserCart).HasForeignKey<ShoppingCart>
                (z => z.OwnerId);
        }
    }
}
